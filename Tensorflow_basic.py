import numpy as np
import tensorflow as tf

tf.reset_default_graph()
x = tf.get_variable("x", shape=(), dtype=tf.float32)
f = x**2
f = tf.Print(f,[x,f], "x, f:")  #for big to small value gradually.

optimizer = tf.train.GradientDescentOptimizer(0.1)
step = optimizer.minimize(f, var_list=[x])


tf.trainable_variables()

s = tf.InteractiveSession()
s.run(tf.global_variables_initializer())

for i in range(10):
    _, curr_x, curr_f = s.run([step, x,f])
    print(curr_x, curr_f)
    
s.close()