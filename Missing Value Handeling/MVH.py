import os
import pandas as pd
import numpy as np
import matplotlib.pyplot as plt

from sklearn.model_selection import train_test_split
from sklearn.tree import DecisionTreeClassifier
from sklearn.metrics import accuracy_score

def build_model(dataset, test_size=0.3, random_state=17):
    X_train, X_test, y_train, y_test = train_test_split(
        dataset.drop('Label', axis=1), dataset.Label,
        test_size=test_size, random_state=random_state)
    
    clf = DecisionTreeClassifier(random_state=random_state).fit(X_train, y_train)
    
    y_pred = clf.predict(X_test)
    return accuracy_score(y_test, y_pred)

df = pd.read_csv('datasets/employee-attrition/employee-attrition-missing.csv')

# This will fail with "ValueError: Input contains NaN, infinity or a value too large for dtype('float32')."
build_model(df)

df.head()

num_orig_rows = len(df)
num_full_rows = len(df.dropna())

(num_orig_rows - num_full_rows)/float(num_orig_rows)

#Solution 1: Remove rows and columns with NaN
df_droprows = df.dropna()
build_model(df_droprows)

df_dropcols = df[['MonthlyIncome','Overtime','Label']]
build_model(df_dropcols)


#Solution 2: Fill NaN values with '-1' sentinel values
df_sentinel = df.fillna(value=-1)
build_model(df_sentinel)


#Solution 3a: Impute values using the mean
from sklearn.preprocessing import Imputer

imp = Imputer(missing_values='NaN', strategy='mean', axis=0)
df_imputed = pd.DataFrame(imp.fit_transform(df),
                          columns=['TotalWorkingYears', 'MonthlyIncome',
                                   'OverTime', 'DailyRate', 'Label'])
build_model(df_imputed)


#Solution 3b: Impute values using the median
imp = Imputer(missing_values='NaN', strategy='median', axis=0)
df_imputed = pd.DataFrame(imp.fit_transform(df),
                          columns=['TotalWorkingYears', 'MonthlyIncome',
                                   'OverTime', 'DailyRate', 'Label'])
build_model(df_imputed)




























