import tensorflow as tf

a = tf.random_uniform([1], seed = 324)
b = tf.random_normal([1], seed = 234)

# Repeatedly running this block with the same graph will generate the same
# sequences of 'a' and 'b'.
print("Session 1")
with tf.Session() as sess1:
  print(sess1.run(a))  # generates 'A1'
  print(sess1.run(a))  # generates 'A2'
  print(sess1.run(b))  # generates 'B1'
  print(sess1.run(b))  # generates 'B2'

print("Session 2")
with tf.Session() as sess2:
  print(sess2.run(a))  # generates 'A1'
  print(sess2.run(a))  # generates 'A2'
  print(sess2.run(b))  # generates 'B1'
  print(sess2.run(b))  # generates 'B2'
  
  '''
  Here, Sets the graph-level random seed.

Operations that rely on a random seed actually derive it from two seeds: the graph-level and operation-level seeds. This sets the graph-level seed.

Its interactions with operation-level seeds is as follows:

If neither the graph-level nor the operation seed is set: A random seed is used for this op.
If the graph-level seed is set, but the operation seed is not: The system deterministically picks an operation seed in conjunction with the
 graph-level seed so that it gets a unique random sequence.
If the graph-level seed is not set, but the operation seed is set: A default graph-level seed and the specified operation seed are used to 
determine the random sequence.
If both the graph-level and the operation seed are set: Both seeds are used in conjunction to determine the random sequence.
  
  '''